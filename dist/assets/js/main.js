(function() {

   
   var typeButton = document.getElementsByClassName('type__item');

   var cards = document.getElementsByClassName('r-card');

	[].forEach.call(typeButton,function(el){
		el.onclick = resetTypes;
	});

	function resetTypes() {
		
		var needType = this.getAttribute('data-type');

		[].forEach.call(typeButton,function(el){
			el.classList.remove('type__item--active');
		});

		[].forEach.call(cards,function(el){
			if (el.getAttribute('data-type') == needType) {
				
				el.classList.remove('r-card--hidden');

			} else {

				el.classList.add('r-card--hidden');
			}
		});

		this.classList.toggle('type__item--active');

		return false;
   	}

   	var sortButtons = document.getElementsByClassName('sort__item');

   	var selectedSorts = [];

   	[].forEach.call(sortButtons,function(el){
		el.onclick = resetSort;
	});

	function resetSort() {

		var needCategory = this.getAttribute('data-category');

		if (selectedSorts.indexOf(needCategory) != -1) {

			var needPosition = selectedSorts.indexOf(needCategory);
			
			selectedSorts.splice(needPosition, 1);

		} else {
			selectedSorts.push(needCategory);
		}


		this.classList.toggle('sort__item--active');

		[].forEach.call(cards,function(el){

			var tags = el.getAttribute('data-categories').split(',');


			if (selectedSorts.length == 0) {

				el.classList.remove('r-card--hidden');
				
			} else {

				for (var i = 0; i < tags.length; i++ ) {

					if (selectedSorts.indexOf(tags[i]) != -1) {
					
						el.classList.remove('r-card--hidden');

					} else {

						el.classList.add('r-card--hidden');
					}
				}
			}
		});

		return false;
	}


	var burger = document.getElementsByClassName('header__burger');

	var menu = document.getElementsByClassName('m-mobile');

   	[].forEach.call(burger,function(el){
		el.onclick = openMenu;
	});

	function openMenu() {

		[].forEach.call(menu,function(el){
			el.classList.toggle('m-mobile--opened');
		});
	}

  
})();